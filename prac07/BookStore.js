let listOfAllKnownAuthors = [] // lists out all the known book authors

// class: Book store
class BookStore
{
    constructor(name, address, owner)
    {
		// Private attributes
        this._name = name;
        this._address = address;
        this._owner = owner;
        this._booksAvailable = [];
        this._totalCopiesOfAllBooks = 0
    }


    addBook(bookInstance, copies)
    {
        let positionOfBook = this.checkForBook(bookInstance); // check for number of books available
		
        if (positionOfBook != null) // if number of books available is not zero
        {
             let foundBook = this._booksAvailable[positionOfBook]; // total number of copies of the book available before adding books
             foundBook.copies += copies;  // total number of copies of the book available after adding books
             console.log("Added " + copies + " copies of " + foundBook.book);
             listOfAllKnownAuthors.push(foundBook.book.author); // pushs the name and author of available books to 'listOfAllKnownAuthors" array
        }
        else
        {
             let bookCopies = 
			 {
                 book: bookInstance,
                 copies: copies
             };
             this._booksAvailable.push(bookCopies); // pushes total number of copies of the books added into "booksAvailable" array
             console.log("Added " + copies + " copies of a new book: " + bookInstance);
        }

        this._totalCopiesOfAllBooks += copies; // total number of copies of all the books added
    }

    sellBook(bookInstance, numberSold)
    {
        let positionOfBook = this.checkForBook(bookInstance); // check for number of books available
		
        if (positionOfBook != null) // if number of books available is not zero
        {
            let foundBook = this._booksAvailable[positionOfBook]; // total number of copies of the book available before selling books
           
		    if (numberSold > this._booksAvailable[positionOfBook].copies) // if number of books neede to be sold is greater than number of books available
            {
                console.log("Not enough copies of " + foundBook.book + " to sell");
            }
            else
            {
                foundBook.copies -= numberSold;
                if (foundBook.copies === 0)
                {
                    this._booksAvailable.pop(PositionOfBook);
                    this._NumTitles -= 1;
                    let foundAuth = this.authorKnown(foundBook.book.author);
                    listOfAllKnownAuthors.pop(foundAuth);
                }
                this._totalCopiesOfAllBooks -= numberSold; // total number of copies of all the books sold
                console.log("Sold " + numberSold + " copies of " + foundBook.book);
            }
        }
        else
        {
            console.log(bookInstance + " not found"); // if no copies of book found
        }
    }

    checkForBook(bookInstance) // check for number of books instance
    {
        let currBookNum = 0;
        while (currBookNum < this._booksAvailable.length) // while current book number is less than number of books available
        {
            if (this._booksAvailable[currBookNum].book.isTheSame(bookInstance))
            {
                return currBookNum;
            }
            else
            {
                return null;
            }
            currBookNum += 1;
        }
        return null;
    }
   
    // Public methods:
    get name()
    {
        return this._name;
    }

    set name(newName)
    {
        this._name = newName;
    }

    get address()
    {
        return this._address;
    }

    set address(newAddress)
    {
        this._address = newAddress;
    }

    get owner()
    {
        return this._owner;
    }

    set address(newOwner)
    {
        this._owner = newOwner;
    }
}

// class Book
class Book
{	
    constructor(title, author, publicationYear, price)
    {
		// Private attributes
        this._title = title;
        this._author = author;
        this._publicationYear = publicationYear;
        this._price = price;
        if (this.authorKnown(this._author) === false)
        {
            listOfAllKnownAuthors.push(this._author)
        }
    }
    
    isTheSame(otherBook)
    {
        return otherBook.price === this.price;
    }
    	
    authorKnown(authorName)
    {
        let foundThem = false;
        for (let pos = 0; pos < listOfAllKnownAuthors.length; pos++)
        {
            if (authorName === listOfAllKnownAuthors[pos])
            {
                foundThem = true;
            }
        }
        return foundThem;
    }

    // Public methods:
    get title()
    {
        return this._title;
    }

    get author()
    {
        return this._author;
    }

    get publicationYear()
    {
        return this._publicationYear;
    }

    get price()
    {
        return this._price;
    }

    toString()
    {
        return this.title + ", " + this.author + ". " + this.publicationYear + " ($" + this.price + ")";
    }
}

// Book details courtesy of Harry Potter series by J.K. Rowling
let cheapSpellBook = new Book("The idiot's guide to spells","Morlan",2005,40);
let flourishAndBlotts = new BookStore("Flourish & Blotts", "North side, Diagon Alley, London, England", "unknown");
let monsterBook = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
let monsterBookToSell = new Book("The Monster Book of Monsters", "Edwardus Lima", 1978, 40);
let spellBook = new Book("The Standard Book of Spells, Grade 4", "Miranda Goshawk", 1921, 80);
flourishAndBlotts.addBook(cheapSpellBook,1000);
flourishAndBlotts.addBook(monsterBook, 500);
flourishAndBlotts.sellBook(monsterBookToSell, 200);
flourishAndBlotts.addBook(spellBook, 40);
flourishAndBlotts.addBook(spellBook, 20);
flourishAndBlotts.sellBook(spellBook, 15);
flourishAndBlotts.addBook(monsterBookToSell, -30);
flourishAndBlotts.sellBook(monsterBookToSell, 750);

console.log("Authors known: " + listOfAllKnownAuthors);
